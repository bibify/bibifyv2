export { default as AuthorField } from '../../components/AuthorField.vue'
export { default as CitationList } from '../../components/CitationList.vue'
export { default as CiteCard } from '../../components/CiteCard.vue'
export { default as Footer } from '../../components/Footer.vue'
export { default as ManualCiteForm } from '../../components/ManualCiteForm.vue'
export { default as SearchBar } from '../../components/SearchBar.vue'
export { default as SourceCard } from '../../components/SourceCard.vue'
export { default as StyleSearchBar } from '../../components/StyleSearchBar.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
